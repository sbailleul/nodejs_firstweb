class LogRow {
    method;
    url;
    _execStart;
    _execEnd;
    _executionTime;

    constructor(method, url, executionTime = null, execStart = null) {
        this.method = method;
        this.url = url;
        this._executionTime = executionTime;
        this._execStart = !execStart ? new Date(Date.now()): execStart;
    }

    get execStart() {
        return this._execStart;
    }

    get execEnd() {
        return this._execEnd;
    }

    set execStart(val) {
        this._execStart = val;
    }

    set execEnd(val) {
        this._execEnd = val;
    }

    get executionTime() {
        const time = new Date(this._execEnd - this._execStart);
        return `${time.getUTCHours()} : ${time.getUTCMinutes()} : ${time.getUTCSeconds()}`;
    }

    toString(){
        return `METHOD : ${this.method} \nURL: ${this.url} \nEXEC TIME: ${this.executionTime} \nHOUR:  ${this.execStart}`;
    }

}
module.exports = LogRow;