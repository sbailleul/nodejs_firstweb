const fs = require('fs');

class Logger{

    static writeLog(path, content){
        return new Promise((resolve, reject) => {
            fs.appendFile(path, content, (err) =>{
                if(err){
                    console.error("Error occurred during file writing");
                    reject(err);
                }
            })
        });
    }
}

module.exports = Logger;