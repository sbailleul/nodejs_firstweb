const LogRow  = require('./LogRow');
const Logger = require('./Logger');
class LogMiddleware {
    _logFilePath;
    _logRow;
    constructor(logFilePath) {
        this.logStart = this.logStart.bind(this);
        this.logAppend = this.logAppend.bind(this);
        this._logFilePath = logFilePath;
    }

    async logStart(req, res, next){
        this._logRow = new LogRow(req.method, req.originalUrl);
        next();
    }

    async logAppend(req, res, next){
        this._logRow.execEnd =  new Date(Date.now());
        try{
            await Logger.writeLog(this._logFilePath, this._logRow.toString());
        }
        catch(error){
            console.error(error);
        }

        next();
    }

}
module.exports = LogMiddleware;