const mysql = require('mysql2');
class EventDb{

    constructor(eventDb) {
    }

    async connect(){
        this.connection = await mysql.createConnectionPromise({
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE,
            port: process.env.DB_PORT
        });
        return this;
    }

    static getInstance(){
        if(!this._EventDb){
            this._EventDb = new EventDb(this);
        }
        return this._EventDb;
    }

    async exec(statement, parameters){
        const res = await this.connection.query(statement, parameters);
        return res;
    }

    disconnect(){
        this.connection.close();
    }

}

module.exports =  EventDb;