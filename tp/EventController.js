const Event = require('./Event');
const ExpressRouteBuilder = require('./ExpressRouterBuilder');
class EventController{
    _eventRepository;
    _httpConstants = require('http2').constants;
    routes = [];

    constructor(eventRepository) {
        this._eventRepository = eventRepository;
        this.getOne = this.getOne.bind(this);
        this.getAll = this.getAll.bind(this);
        this.add = this.add.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
        this.routes = [
            {url: '/events', httpMethod: this._httpConstants.HTTP2_METHOD_POST, middleware:[this.add]},
            {url: '/events', httpMethod: this._httpConstants.HTTP2_METHOD_GET, middleware:[this.getAll]},
            {url: '/events/:id', httpMethod: this._httpConstants.HTTP2_METHOD_GET, middleware:[this.getOne]},
            {url: '/events/:id', httpMethod: this._httpConstants.HTTP2_METHOD_DELETE, middleware:[this.delete]},
            {url: '/events/:id', httpMethod: this._httpConstants.HTTP2_METHOD_PUT, middleware:[this.update]},
            {url: '/events/:id', httpMethod: this._httpConstants.HTTP2_METHOD_PATCH, middleware:[this.update]}];
    }

    async add(req, res, next){
        let event = req.body;
        console.log(event);
        if(event.date && event.name && event.price){
            event = new Event(undefined, event.name, event.date, event.price);
        } else {
            return res.status(this._httpConstants.HTTP_STATUS_UNPROCESSABLE_ENTITY).end();
        }
        const eventId = await this._eventRepository.add(event);
        const creationLocation = ExpressRouteBuilder.fullUrl(req).addPath(eventId).build();
        res.status(this._httpConstants.HTTP_STATUS_CREATED).header(this._httpConstants.HTTP2_HEADER_LOCATION,creationLocation ).end();
        next();
    }

    async getOne(req, res, next){
        const id = parseInt(req.params.id);
        const entity =  await this._eventRepository.getOneById(id);
        if(entity){
            res.status(this._httpConstants.HTTP_STATUS_OK).send(entity);
        }else {
            res.status(this._httpConstants.HTTP_STATUS_NOT_FOUND).end();
        }
        next();
    }

    async getAll(req, res, next){
        const entities = await this._eventRepository.getAll();
        res.status(this._httpConstants.HTTP_STATUS_OK).send(entities);
        next();
    }

    async delete(req, res, next){
        const {id} = parseInt(req.params);
        if(! await this._eventRepository.eventExists(id)){
            res.status(this._httpConstants.HTTP_STATUS_NOT_FOUND).end();
        }
        this._eventRepository.delete(id);
        res.status(this._httpConstants.HTTP_STATUS_NO_CONTENT).end();
        next();
    }

    async update(req, res, next){
        let event = req.body;
        const id = parseInt(req.params.id);
        if(! await this._eventRepository.eventExists(id)){
            res.status(this._httpConstants.HTTP_STATUS_NOT_FOUND).end();
        }
        if(event.date && event.name && event.price){
            event = new Event(undefined, event.name, event.date, event.price);
        } else {
            res.status(this._httpConstants.HTTP_STATUS_UNPROCESSABLE_ENTITY).end();
        }
        this._eventRepository.update(event, id);
        res.status(this._httpConstants.HTTP_STATUS_NO_CONTENT).end();
        next();
    }
}

module.exports = EventController;