module.exports = class ExpressRouteBuilder{

    _fullUrl;
    static _instance;

    static fullUrl(req){
        this._instance = new ExpressRouteBuilder();
        this._instance._fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        return this._instance;
    }

    addPath(path){
        `${path}`.replace(/^\/+|\/+$/g, '');
        this._fullUrl+= `/${path}`;
        return this;
    }

    build(){
        return this._fullUrl;
    }

};