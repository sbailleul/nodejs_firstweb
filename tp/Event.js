class Event{
    id;
    name;
    date;
    price;
    constructor(id, name, date, price) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.price = price;
    }
}

module.exports = Event;