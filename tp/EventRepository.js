class EventRepository{
    _db;

    constructor(db) {
        this._db = db;
    }

    async eventExists(id){
        return await this.getOneById(id);
    }
    async getOneById(id){
        const res =  await this._db.exec('SELECT * FROM event WHERE id = ?', [id])
        return res[0];
    }

    async getAll(){
        const res = await this._db.exec('SELECT * FROM event');
        return res[0];
    }

    async add(event){
        const res = await this._db.exec('INSERT INTO event (name, date, price) VALUES (?, ?, ?)',[event.name, event.date, event.price]);
        return res[0].insertId;
    }

    delete(id){
        this._db.exec('DELETE FROM event WHERE id=?', [id]);
    }

    update(event, id){
        this._db.exec('UPDATE event SET name=?, date=?, price=? WHERE id=?', [event.name, event.date, event.price, id]);
    }

}


module.exports = EventRepository;